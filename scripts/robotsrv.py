#!/usr/bin/env python
from __future__ import print_function
import struct
import sys
import rospy
import baxter_interface
import socket
import time
from baxter_interface import CHECK_VERSION
from IKmod import IKsolve
from mover import Mover
from baxter_pykdl import baxter_kinematics
import numpy as np
import math
import string
from contextlib import closing
from std_srvs.srv import Empty
import re
from reflex_msgs.msg import Command
from reflex_msgs.msg import PoseCommand
from reflex_msgs.msg import VelocityCommand
from reflex_msgs.msg import Hand
from reflex_msgs.msg import FingerPressure
from reflex_msgs.srv import SetTactileThreshold, SetTactileThresholdRequest

class server:
    def __init__(self):

        baxter = baxter_interface.RobotEnable()
        baxter.enable()
        self.limb = "right"
        self.right = baxter_interface.Limb(self.limb)
        self.right.set_joint_position_speed(0.3)
        self.control_cmd = dict()
        print("set neutral position...")
        baxter_interface.Limb.move_to_neutral(self.right)
        print("OK")
        self.IKservice = IKsolve(self.limb)
        #self.count = 0
        #self.mover = Mover()
        #self.t = 0
        rospy.Rate(1000)

        # Services can automatically call hand calibration
        self.calibrate_fingers = rospy.ServiceProxy('/reflex_takktile/calibrate_fingers', Empty)
        self.calibrate_tactile = rospy.ServiceProxy('/reflex_takktile/calibrate_tactile', Empty)

        # Services can set tactile thresholds and enable tactile stops
        self.enable_tactile_stops = rospy.ServiceProxy('/reflex_takktile/enable_tactile_stops', Empty)
        self.disable_tactile_stops = rospy.ServiceProxy('/reflex_takktile/disable_tactile_stops', Empty)
        self.set_tactile_threshold = rospy.ServiceProxy('/reflex_takktile/set_tactile_threshold', SetTactileThreshold)

        # This collection of publishers can be used to command the hand
        self.command_pub = rospy.Publisher('/reflex_takktile/command', Command, queue_size=1)
        self.pos_pub = rospy.Publisher('/reflex_takktile/command_position', PoseCommand, queue_size=1)
        self.vel_pub = rospy.Publisher('/reflex_takktile/command_velocity', VelocityCommand, queue_size=1)

        # Constantly capture the current hand state
        #rospy.Subscriber('/reflex_takktile/hand_state', Hand, hand_state_cb)

    def mover(self):
        #self.coordinate = [x,y,z]
        self.limb_joints = self.IKservice.solve(self.cdn_dict[0])

        if (self.cmd_name == "self_grasp_object"):
            self.self_grasp_object(self.t,self.s,self.w)


        elif (self.cmd_name == "receive_object"):
            self.receive_object(self.t,self.s,self.w)


        elif (self.cmd_name == "release_object"):
            self.release_object(self.t,self.s,self.w)


        elif (self.cmd_name == "point_object"):
            self.point_object(self.t)


        elif (self.cmd_name == "move_arm_to_point"):
            self.move_arm_to_point(self.t)


        elif (self.cmd_name == "yamove_arm_to_point"):
            self.yamove_arm_to_point(self.t)


        elif (self.cmd_name == "open_hand"):
            self.open_hand(self.s,self.w)


        elif (self.cmd_name == "catch_object"):
            self.catch_object(self.s,self.w)


        elif (self.cmd_name == "gotoready"):
            self.gotoready(self.s,self.w,self.r)


        elif(self.cmd_name == "nullfunc"):
            self.nullfunc()


        elif (self.cmd_name == "init_hand"):
            self.init_hand()


        elif (self.cmd_name == "terminate"):
            self.terminate()


        else:
            return False

    def self_grasp_object(self,t,s,w):
        #cdn = [depthx,depthy,depthz]
        if (self.limb_joints != 0):
            self.right.move_to_joint_positions(self.limb_joints)
        else:
            return 0
        time.sleep(t/1000)
        self.grasp()
        depth = self.IKservice.solve(self.cdn_dict[1])
        if (depth != 0):
            self.right.move_to_joint_positions(depth)
        else:
            return 0



    def receive_object(self,t,w,s):
        #cdn = [depthx,depthy,depthz]
        if (self.limb_joints != 0):
            self.right.move_to_joint_positions(self.limb_joints)
        else:
            return 0
        time.sleep(t/1000)
        self.grasp()
        depth = self.IKservice.solve(self.cdn_dict[1])
        if (depth != 0):
            self.right.move_to_joint_positions(depth)
        else:
            return 0

    def release_object(self,t,w,s):
        #cdn = [depthx,depthy,depthz]
        if (self.limb_joints != 0):
            self.right.move_to_joint_positions(self.limb_joints)
        else:
            return 0
        time.sleep(t/1000)
        self.open()
        depth = self.IKservice.solve(self.cdn_dict[1])
        if (depth != 0):
            self.right.move_to_joint_positions(depth)
        else:
            return 0



    def point_object(self,t):
        if (self.limb_joints != 0):
            self.right.move_to_joint_positions(self.limb_joints)
        else:
            return 0
            time.sleep(t/1000)
        #buttai wo yubisasu



    def move_arm_to_point(self,t):
        if (self.limb_joints != 0):
            self.right.move_to_joint_positions(self.limb_joints)
        else:
            return 0
        time.sleep(t/1000)


    def yamove_arm_to_point(self,t):
        cmd_dict = []
        pose = self.right.endpoint_pose()
        pt_cur = str(pose["position"]).translate(string.maketrans("",""),
                        "xyz= ()Point").split(",")
        pt_cur[1]=float(pt_cur[1])
        pt_cur[2]=float(pt_cur[2])
        pt_cur[0]=float(pt_cur[0])
        dif = self.cdn_dict[0][1] + pt_cur[1]
        top = pt_cur[1] - dif/2
        top = float(abs(top))

        while top < abs(pt_cur[1]):
            pt_cur[1] = abs(pt_cur[1])
            pt_cur[1] -= 0.01
            pt_cur[2] += 0.01
            cmd = self.IKservice.solve(pt_cur)
            if(cmd != 0):
                self.right.set_joint_positions(cmd)
        while top > abs(pt_cur[1]) and abs(self.coordinate[1]) < abs(pt_cur[1]):
            pt_cur[1] = abs(pt_cur[1])
            pt_cur[1] -= 0.01
            pt_cur[2] -= 0.01
            cmd = self.IKservice.solve(pt_cur)
            if(cmd != 0):
                self.right.set_joint_positions(cmd)

    def open_hand(self):
        self.open()
        print("mada")


    def catch_object(self):
        self.grasp()
        print("mada")


    def gotoready(self,s,w,r):
        self.right.move_to_neutral()
        self.open()


    def nullfunc(self):
        None

    def init_hand(self):
        self.open()
        print("MADA")

    def terminate(self):
        rospy.is_shutdown()


    def make_command(self,msg):
        x,y,z,depthx,depthy,depthz,self.t,self.s,self.w,self.r = 0,0,0,0,0,0,0,0,0,0
        self.keyt,self.keys,self.keyw,self.keyr = None,None,None,None
        msgspl = msg.split(" ")
        for i in range(int(len(msgspl))):

            if(re.match(r"-t",msgspl[i])):
                con = msgspl[i].split("-t")
                #print(con)
                self.t = con[1]
                self.keyt = msgspl[i]
            elif(re.match(r"-s",msgspl[i])):
                con = msgspl[i].split("-s")
                #print(con)
                self.s = con[1]
                self.keys = msgspl[i]
            elif(re.match(r"-w",msgspl[i])):
                con = msgspl[i].split("-w")
                #print(con)
                self.w = con[1]
                self.keyw = msgspl[i]
            elif(re.match(r"-r",msgspl[i])):
                con = msgspl[i].split("-r")
                #print(con)
                self.r = con[1]
                self.keyr = msgspl[i]
            print(msgspl)

        else:

            if(self.keyt != None):
                msgspl.remove(self.keyt)
            if(self.keys != None):
                msgspl.remove(self.keys)
            if(self.keyw != None):
                msgspl.remove(self.keyw)
            if(self.keyr != None):
                msgspl.remove(self.keyr)

        self.cmd_name = msgspl[0]
        self.cdn_dict = []
        print(msgspl)
        for i in range(1,(len(msgspl)-1),3):
            print(i)
            cdn = [float(msgspl[i]),float(msgspl[i+1]),float(msgspl[i+2])]
            self.cdn_dict.append(cdn)
        float(self.t)
        float(self.s)
        float(self.w)
        float(self.r)
        self.mover()

    def grasp(self):
        self.pos_pub.publish(VelocityCommand(f1 = 2, f2 = 2, f3 = 2, preshape = 0.0))

    def open(self):
        self.pos_pub.publish(VelocityCommand(f1 = 0, f2 = 0, f3 = 0, preshape = 0.0))

def main(srv):
    host = '127.0.0.1'
    port = 12345
    backlog = 10
    bufsize = 4096

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(None)
    with closing(sock):
        sock.bind((host, port))
        sock.listen(backlog)
        while True:
            conn, address = sock.accept()
            with closing(conn):
                msg = conn.recv(bufsize)

                srv.make_command(msg)

                conn.send(msg)

    return

if __name__ == "__main__":
    rospy.init_node("robotsrv")
    srv = server()
    cmd = main(srv)
    #service_server = rospy.Service("control_command",sh_robot,srv.ik_response)
    #rospy.spin()
