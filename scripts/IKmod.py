import struct
import sys

import baxter_interface
import rospy
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)
from std_msgs.msg import Header
from baxter_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)


class IKsolve:
    def __init__(self,arm):
        self.limb = arm
        ns = "ExternalTools/" + self.limb + "/PositionKinematicsNode/IKService"
        rospy.wait_for_service(ns, 5.0)
        self.iksvc = rospy.ServiceProxy(ns, SolvePositionIK)


    def solve(self,coordinate):

        ikreq = SolvePositionIKRequest()
        hdr = Header(stamp=rospy.Time.now(), frame_id='base')
        poses = {
            'left': PoseStamped(
            header=hdr,
            pose=Pose(
                position=Point(
                    #length
                    x=coordinate[0],
                    #width
                    y=coordinate[1],
                    #height
                    z=coordinate[2],
                ),
                orientation=Quaternion(
                    x=-0.366894936773,
                    y=0.885980397775,
                    z=0.108155782462,
                    w=0.262162481772,
                ),
            ),
        ),
            'right': PoseStamped(
                header = hdr,
                pose=Pose(
                    position=Point(
                        #length
                        x=coordinate[0],
                        #width
                        y=-coordinate[1],
                        #height
                        z=coordinate[2],
                    ),
                    orientation=Quaternion(
                        x=0.367048116303,
                        y=0.885911751787,
                        z=-0.108908281936,
                        w=0.261868353356,
                    ),
                ),
            ),
        }

        ikreq.pose_stamp.append(poses[self.limb])
        try:
            resp = self.iksvc(ikreq)
        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))
            return 1
        resp_seeds = struct.unpack('<%dB' % len(resp.result_type),
                                resp.result_type)
        if (resp_seeds[0] != resp.RESULT_INVALID):
            seed_str = {
                        ikreq.SEED_USER: 'User Provided Seed',
                        ikreq.SEED_CURRENT: 'Current Joint Angles',
                        ikreq.SEED_NS_MAP: 'Nullspace Setpoints',
                        }.get(resp_seeds[0], 'None')
            print("SUCCESS - Valid Joint Solution Found from Seed Type: %s" %
                        (seed_str,))
            self.limb_joints = dict(zip(resp.joints[0].name,
                                        resp.joints[0].position))
            print ("Response Message:\n", resp)
            return self.limb_joints
        else:
            print("INVALID POSE - No Valid Joint Solution Found.")
            return 0
