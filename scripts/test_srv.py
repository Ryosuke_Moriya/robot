#usr/bin/env python
#coding:utf-8
from __future__ import print_function
import socket
from contextlib import closing

def main():
  host = '127.0.0.1'
  port = 4000
  backlog = 10
  bufsize = 4096

  sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  with closing(sock):
    sock.bind((host, port))
    sock.listen(backlog)
    while True:
      conn, address = sock.accept()
      with closing(conn):
        msg = conn.recv(bufsize)
        strmsg = str(msg)
        print(strmsg)
        conn.send(strmsg)
        if(strmsg == "terminate"):
            break
    conn.close()
    sock.close()
  return

if __name__ == '__main__':
  main()
