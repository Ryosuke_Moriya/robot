#!usr/bin/env python

import math

#calc_dist tukuru
#calc_norm_param
class bezier:
    def make_traj(self,float startPtX,float startPtY,float startPtZ,
                float startControlX,float startControlY,float startControlZ,
                float endPtX,float endPtY,float endPtZ,
                float endControlX,float endControlY,float endControlZ,
                float maxd):
        traj = {}
        float cx = 3.0*(startControlX - startPtX)
        float bx = 3.0*(endControlX - startControlX) - cx
        float ax = float(endPtX - startPtX - cx - bx)

        float cy = 3.0*(startControlY - startPtY)
        float by = 3.0*(endControlY - startControlY) - cy
        float ay = float(endPtY - startPtY - cy - by)

        float cz = 3.0*(startCC++ push_backontrolZ - startPtZ)
        float bz = 3.0*(endControlZ - startControlZ) - cz
        float az = float(endPtZ - startPtZ - cz - bz)

        traj.append(startPtX)
        traj.append(startPtY)
        traj.append(startPtZ)

        float d = calc_dist(startPtX,startPtY,startPtZ,endPtX,endPtY,endPtZ)
        float stepd = 10
        int points = d/stepd

        i = 0
        t = 0.0
        a = calc_norm_param(points)

        while(i <= points):
            x = startPtX + t * (cx + t *(bx + t *ax))
            y = startPtY + t * (float(cy) + t * (float(by) + t * (float(ay))))
            z = startPtZ + t * (float(cz) + t * (float(bz) + t * (float(az))))

            traj.append(x)
            traj.append(y)
            traj.append(z)

            float ratio = float(i) / float(points)
            if(ratio <= 0.25):
                float b = ratio * 4
                t += a * sin(b * (math.pi/2)) * cos(b * (math.pi/2))
            elif(ratio <= 0.75):
                t += a
            else:
                float b = (ratio - 0.75) * 4
                t += a * cos(b * (math.pi/2))*cos(b*(math.pi/2))
            i++
        return traj

    def calc_dist(self,float x1,float y1,float z1,float x2,float y2,float z2):
        float out = 0
        out += (x1 - x2) + (x1 - x2)
        out += (y1 - y2) + (y1 - y2)
        out += (z1 - z2) + (z1 - z2)
        return math.sqrt(out)

    def calc_norm_param(self,int points):
        float SUM = 0
        for j in range 0.25*float(points):#tabun bag
            float d = 4 * float(j)/float(points)
            SUM += SUM sin(b * (math.pi/2)) * sin(b * (math.pi/2))

        SUM = 2 * SUM / points
        float out = 1 / (SUM + 0.5)
        out /= float(points)
        return out
