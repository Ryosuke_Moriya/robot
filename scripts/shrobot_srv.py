#!/usr/bin/env python
import struct
import sys
import rospy
import baxter_interface
from sh_robot.srv import(
    sh_robot,
    sh_robotResponse
)
import time
from baxter_interface import CHECK_VERSION
from IKmod import IKsolve
from mover import Mover
from baxter_pykdl import baxter_kinematics
import numpy as np
import math
import string

class server:
    def __init__(self):

        baxter = baxter_interface.RobotEnable()
        baxter.enable()
        self.limb = "right"
        self.right = baxter_interface.Limb(self.limb)
        self.right.set_joint_position_speed(0.3)
        self.control_cmd = dict()
        print("set neutral position...")
        baxter_interface.Limb.move_to_neutral(self.right)
        print("OK")
        self.IKservice = IKsolve(self.limb)
        #self.count = 0
        self.mover = Mover()
        #self.t = 0


    def ik_response(self,req):
        self.coordinate = [req.x,req.y,req.z]
        self.limb_joints = self.IKservice.solve(self.coordinate)

        if (req.cmd_name == "self_grasp_object"):
            self.self_grasp_object(req.depthx,req.depthy,req.depthz,req.t,req.s,req.w)
            return sh_robotResponse(True)

        elif (req.cmd_name == "receive_object"):
            self.receive_object(req.depthx,req.depthy,req.depthz,req.t,req.s,req.w)
            return sh_robotResponse(True)

        elif (req.cmd_name == "release_object"):
            self.release_object(req.depthx,req.depthy,req.depthz,req.t,req.s,req.w)
            return sh_robotResponse(True)

        elif (req.cmd_name == "point_object"):
            self.point_object(req.t)
            return sh_robotResponse(True)

        elif (req.cmd_name == "move_arm_to_point"):
            self.move_arm_to_point(req.t)
            return sh_robotResponse(True)

        elif (req.cmd_name == "yamove_arm_to_point"):
            self.yamove_arm_to_point(req.t)
            return sh_robotResponse(True)

        elif (req.cmd_name == "open_hand"):
            self.open_hand(req.s,req.w)
            return sh_robotResponse(True)

        elif (req.cmd_name == "catch_object"):
            self.catch_object(req.s,req.w)
            return sh_robotResponse(True)

        elif (req.cmd_name == "gotoready"):
            self.gotoready(req.s,req.w,req.r)
            return sh_robotResponse(True)

        elif(req.cmd_name == "nullfunc"):
            self.nullfunc()
            return sh_robotResponse(True)

        elif (req.cmd_name == "init_hand"):
            self.init_hand()
            return sh_robotResponse(True)

        elif (req.cmd_name == "terminate"):
            self.terminate()
            return sh_robotResponse(True)

        else:
            return sh_robotResponse(False)

    def self_grasp_object(self,depthx,depthy,depthz,t,s,w):
        cdn = [depthx,depthy,depthz]
        print(self.limb_joints)
        if (self.limb_joints != 0):
            self.right.move_to_joint_positions(self.limb_joints)
        else:
            return sh_robotResponse(False)
        time.sleep(t/1000)
        #buttai wo tukamu
        depth = self.IKservice.solve(cdn)
        if (depth != 0):
            self.right.move_to_joint_positions(depth)
        else:
            return sh_robotResponse(False)



    def receive_object(self,depthx,depthy,depthz,t,w,s):
        cdn = [depthx,depthy,depthz]
        if (self.limb_joints != 0):
            self.right.move_to_joint_positions(self.limb_joints)
        else:
            return sh_robotResponse(False)
        time.sleep(t/1000)
        #hito no te kara buttai wo uketoru
        depth = self.IKservice.solve(cdn)
        if (depth != 0):
            self.right.move_to_joint_positions(depth)
        else:
            return sh_robotResponse(False)

    def release_object(self,depthx,depthy,depthz,t,w,s):
        cdn = [depthx,depthy,depthz]
        if (self.limb_joints != 0):
            self.right.move_to_joint_positions(self.limb_joints)
        else:
            return sh_robotResponse(False)
        time.sleep(t/1000)
        #buttai wo hanasu
        depth = self.IKservice.solve(cdn)
        if (depth != 0):
            self.right.move_to_joint_positions(depth)
        else:
            return sh_robotResponse(False)



    def point_object(self,t):
        if (self.limb_joints != 0):
            self.right.move_to_joint_positions(self.limb_joints)
        else:
            return sh_robotResponse(False)
            time.sleep(t/1000)
        #buttai wo yubisasu



    def move_arm_to_point(self,t):
        if (self.limb_joints != 0):
            self.right.move_to_joint_positions(self.limb_joints)
        else:
            return sh_robotResponse(False)
        time.sleep(t/1000)


    def yamove_arm_to_point(self,t):




        cmd_dict = []
        pose = self.right.endpoint_pose()
        pt_cur = str(pose["position"]).translate(string.maketrans("",""),
                        "xyz= ()Point").split(",")
        pt_cur[1]=float(pt_cur[1])
        pt_cur[2]=float(pt_cur[2])
        pt_cur[0]=float(pt_cur[0])
        dif = self.coordinate[1] + pt_cur[1]
        top = pt_cur[1] - dif/2
        top = float(abs(top))

        while top < abs(pt_cur[1]):
            pt_cur[1] = abs(pt_cur[1])
            pt_cur[1] -= 0.01
            pt_cur[2] += 0.01
            cmd = self.IKservice.solve(pt_cur)
            if(cmd != 0):
                self.right.set_joint_positions(cmd)
        while top > abs(pt_cur[1]) and abs(self.coordinate[1]) < abs(pt_cur[1]):
            pt_cur[1] = abs(pt_cur[1])
            pt_cur[1] -= 0.01
            pt_cur[2] -= 0.01
            cmd = self.IKservice.solve(pt_cur)
            if(cmd != 0):
                self.right.set_joint_positions(cmd)

    def open_hand(self):
        #te wo hiraku
        print("mada")


    def catch_object(self):
        #tukamu
        print("mada")


    def gotoready(self,s,w,r):
        self.right.move_to_neutral()
        #hand wo hirogeru


    def nullfunc(self):
        None

    def init_hand(self):
        #hand wo syokika
        print("MADA")

    def terminate(self):
        rospy.is_shutdown()



if __name__ == "__main__":
    rospy.init_node("a")
    srv = server()
    service_server = rospy.Service("control_command",sh_robot,srv.ik_response)
    rospy.spin()
