#!/usr/bin/env python
#coding:utf-8
from __future__ import print_function
import rospy
import sys

from sh_robot.srv import sh_robot
import argparse

import socket

from contextlib import closing

class sh_robot_client:
    def __init__(self):
        rospy.init_node("Point_client")
        self.control_command = rospy.ServiceProxy("control_command",sh_robot)
        self.host = "127.0.0.1"
        self.port = 4000
        self.backlog = 10
        self.bufsize = 4096


    def main(self):
        """
        parser = argparse.ArgumentParser(description = "sh-robot client")
        parser.add_argument("cmdname",type = str,action = "store")
        parser.add_argument("-t",type = int,action = "store",default = 0)
        parser.add_argument("-s",type = float,action = "store",default = 0.0)
        parser.add_argument("-w",type = float,action = "store",default = 0.0)
        parser.add_argument("x",type = float,action = "store",nargs = "?")
        parser.add_argument("y",type = float,action = "store",nargs = "?")
        parser.add_argument("z",type = float,action = "store",nargs = "?")
        parser.add_argument("depthx",type = float,action = "store",nargs = "?",
                            default = 0.0)
        parser.add_argument("depthy",type = float,action = "store",nargs = "?",
                            default = 0.0)
        parser.add_argument("depthz",type = float,action = "store",nargs = "?",
                            default = 0.0)
        parser.add_argument("-r",type = float,action = "store",default = 0.0)
        args = parser.parse_args()
        cmd_name,t,s = args.cmdname,args.t,args.s
        w,x,y,z = args.w,args.x,args.y,args.z
        depthx,depthy,depthz,r = args.depthx,args.depthy,args.depthz,args.r
        """
        x = 0.0
        y = 0.0
        z = 0.0
        depthx = 0.0
        depthy = 0.0
        depthz = 0.0
        t = 0
        s = 0.0
        w = 0.0
        r = 0.0
        sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        with closing(sock):
            sock.bind((self.host,self.port))
            sock.listen(self.backlog)
            while True:
                conn,address = sock.accept()
                with closing(conn):
                    msg = conn.recv(self.bufsize)
                    a = msg.split(" ")
                    length = len(a)
                    cmd_name = a[0]
                    if(length >= 2):
                        x = a[1]
                    if(length >= 3):
                        y = a[2]
                    if(length >= 4):
                        z = a[3]
                    conn.send(msg)
                    print (cmd_name,x,y,z,depthx,depthy,depthz,t,s,w,r)
                    response = self.control_command(str(cmd_name),float(x),float(y),float(z),depthx,depthy,depthz,t,s,w,r)
                    if response.success:
                        rospy.loginfo("send command success.")
                    else:
                        rospy.logerr("send command failed.")
        return
if __name__ == "__main__":
    clt = sh_robot_client()
    clt.main()
